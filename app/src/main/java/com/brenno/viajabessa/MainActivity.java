package com.brenno.viajabessa;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView listview;
    final String url = "https://private-af0c7-viajabessa84.apiary-mock.com/opcoes";

    List<PacoteViagemModelo> alist;
    MyAdapter adapter;
    RequestQueue requestQueue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listview = findViewById(R.id.listview);
        alist = new ArrayList<>();
        requestQueue = Volley.newRequestQueue(this);

        //Populando lista com dados do JSON do Apiary
        getdata();

        adapter = new MyAdapter(this, R.layout.customcell, alist);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Fazendo POST quando o usuário clicar em algo da lista
                postData();

                // Usando uma classe singleton para guardar
                // as informações do item para mostrar na outra tela
                // intent.putExtra() não estava funcionando por algum motivo.
                Singleton.pacote = alist.get(position);

                Intent intent = new Intent(view.getContext(), InformacoesPacote.class);
                startActivity(intent);


            }
        });
    }



    private void getdata ()
    {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                JSONArray jsonArray = response;
                try {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String nome = jsonObject.getString("nome");
                        String imagem = jsonObject.getString("foto");
                        double valor = jsonObject.getDouble("valor");
                        String descricao = jsonObject.getString("descricao");

                        alist.add(new PacoteViagemModelo(nome, imagem, descricao, valor));
                    }
                    adapter.notifyDataSetChanged();
                } catch (Exception w) {
                    Toast.makeText(MainActivity.this, w.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        requestQueue.add(jsonArrayRequest);
    }


    public void postData() {

        requestQueue = Volley.newRequestQueue(getApplicationContext());
        JSONObject object = new JSONObject();
        try {
            object.put("osversion",System.getProperty("os.version"));
            object.put("device",android.os.Build.DEVICE);
            object.put("model",android.os.Build.MODEL + " (" + android.os.Build.PRODUCT + ")");

            // Para mostrar os dados do smartphone do usuário em uma mensagem
            // Toast para testes
            Toast.makeText(MainActivity.this,
                    object.toString() , Toast.LENGTH_SHORT).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(MainActivity.this, "String Response : "+ response.toString() , Toast.LENGTH_SHORT).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Error getting response" , Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(jsonObjectRequest);
    }


}