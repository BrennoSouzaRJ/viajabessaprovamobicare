package com.brenno.viajabessa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapter extends ArrayAdapter<PacoteViagemModelo> {
    Context context;
    int resource;
    List<PacoteViagemModelo> pacotesList;
    MyAdapter(Context context,int resource,List<PacoteViagemModelo> pacotesList)
    {
        super(context,resource,pacotesList);
        this.context = context;
        this.resource = resource;
        this.pacotesList = pacotesList;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(resource,null,false);
        TextView aname = view.findViewById(R.id.pacotenome);
        ImageView aimage = view.findViewById(R.id.pacoteimagem);
        PacoteViagemModelo pacoteViagemModelo = pacotesList.get(position);
        aname.setText(pacoteViagemModelo.getPacotenome());
        Picasso.get().load(pacoteViagemModelo.getPacoteimagem()).into(aimage);
        return view;
    }
}
