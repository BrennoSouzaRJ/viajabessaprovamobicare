package com.brenno.viajabessa;

public class PacoteViagemModelo {

    public String pacotenome;
    public String pacoteimagem;
    public String pacotedescricao;
    public double pacotevalor;

    public PacoteViagemModelo(String pacotenome, String pacoteimagem, String pacotedescricao, double pacotevalor) {
        this.pacotenome = pacotenome;
        this.pacoteimagem = pacoteimagem;
        this.pacotedescricao = pacotedescricao;
        this.pacotevalor = pacotevalor;
    }

    public String getPacotenome() {
        return pacotenome;
    }

    public void setPacotenome(String pacotenome) {
        this.pacotenome = pacotenome;
    }

    public String getPacoteimagem() {
        return pacoteimagem;
    }

    public void setPacoteimagem(String pacoteimagem) {
        this.pacoteimagem = pacoteimagem;
    }

    public String getPacotedescricao() {
        return pacotedescricao;
    }

    public void setPacotedescricao(String pacotedescricao) {
        this.pacotedescricao = pacotedescricao;
    }

    public double getPacotevalor() {
        return pacotevalor;
    }

    public void setPacotevalor(double pacotevalor) {
        this.pacotevalor = pacotevalor;
    }
}
