package com.brenno.viajabessa;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

public class InformacoesPacote extends AppCompatActivity {

    ImageView mainImageView;
    TextView title, description, preco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacoes_pacote);

        mainImageView = findViewById(R.id.mainImageView);
        title = findViewById(R.id.titulo);
        description = findViewById(R.id.description);
        preco = findViewById(R.id.preco);

        if (Singleton.pacote != null) {
            Picasso.get().load(Singleton.pacote.getPacoteimagem()).into(mainImageView);
            title.setText(Singleton.pacote.getPacotenome());
            description.setText(Singleton.pacote.getPacotedescricao());


            // Formatação do valor double para R$
            // poderiamos formatar para qualquer tipo de moeda
            // usando o local/linguagem do usuário atual
            Locale localeBR = new Locale( "pt", "BR" );
            NumberFormat dinheiroBR = NumberFormat.getCurrencyInstance(localeBR);
            preco.setText(dinheiroBR.format(Singleton.pacote.getPacotevalor()));

        }
    }
}